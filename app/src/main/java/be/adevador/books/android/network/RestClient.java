package be.adevador.books.android.network;

import android.util.Base64;

import com.squareup.okhttp.Authenticator;
import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.Proxy;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RestClient {

    private static BooksApi restClient;

    private static final String ROOT = "http://192.168.1.92:8080";

    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static BooksApi getRestClient() {
        return restClient;
    }

    public static void setupRestClient() {

//        OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.setAuthenticator(new Authenticator() {
//            @Override
//            public Request authenticate(Proxy proxy, Response response) throws IOException {
//                System.out.println("Authenticating for response: " + response);
//                System.out.println("Challenges: " + response.challenges());
//
//                String base64EncodedClientAndSecret = "Basic " + Base64.encodeToString("books_password_client:123456".getBytes(), Base64.NO_WRAP);
//
//                RequestBody requestBody = new FormEncodingBuilder()
//                        .add("grant_type","password")
//                        .add("username", "roy")
//                        .add("password", "spring")
//                        .add("scope", "read write")
//                        .add("client_id", "books_password_client")
//                        .add("client_secret", "123456")
//                        .build();
//
//                Request request = response.request().newBuilder()
//                        .header("Authorization", base64EncodedClientAndSecret)
//                        .header("Content-Type", "application/x-www-form-urlencoded")
//                        .post(requestBody)
//                        .build();
//
//                return request;
//
//            }
//
//            @Override
//            public Request authenticateProxy(Proxy proxy, Response response) throws IOException {
//                return null;
//            }
//        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        restClient = retrofit.create(BooksApi.class);
    }
}
