package be.adevador.books.android.network;

import be.adevador.books.android.model.AuthenticationResponse;
import be.adevador.books.android.model.Ip;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;

public interface BooksApi {

    @GET("/greeting")
    Call<Ip> getIp();

    @FormUrlEncoded
    @POST("/oauth/token")
    Call<AuthenticationResponse> getAccessToken(@Header("Authorization") String authorization,
                            @Field("username") String username,
                            @Field("password") String password,
                            @Field("grant_type") String grantType);

}
