package be.adevador.books.android.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import be.adevador.books.android.R;
import be.adevador.books.android.model.AuthenticationResponse;
import be.adevador.books.android.model.Ip;
import be.adevador.books.android.network.RestClient;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class LoginActivity extends AppCompatActivity {

    @OnClick(R.id.activity_login_btn_sign_in) void signIn() {
        Call<Ip> call = RestClient.getRestClient().getIp();
        call.enqueue(new Callback<Ip>() {
            @Override
            public void onResponse(Response<Ip> response) {
                // Get result Repo from response.body()
                Ip ip = response.body();
                System.out.println();
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println();
            }
        });
    }

    @OnClick(R.id.activity_login_btn_get_token) void getAccessToken() {

        String base64EncodedClientAndSecret = "Basic " + Base64.encodeToString("books_password_client:123456".getBytes(), Base64.NO_WRAP);

        Call<AuthenticationResponse> call = RestClient.getRestClient().getAccessToken(base64EncodedClientAndSecret, "roy", "spring", "password");
        call.enqueue(new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Response<AuthenticationResponse> response) {
                // Get result Repo from response.body()
                AuthenticationResponse authenticationResponse = response.body();
                System.out.println();
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
